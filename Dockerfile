FROM ubuntu:23.10

ARG WORK_DIR
ARG INSTALL_DIR
ARG GEANT4_VERSION
ARG XercesC_VERSION
ARG Coin_VERSION

#WORKDIR $WORK_DIR

### Install some basic components: git, wget..

RUN apt-get update -qq && \
    apt-get install -y -qq \
    apt-utils \
    git \
    wget \
    unzip \
    build-essential \
    freeglut3-dev \
    mercurial \
    nlohmann-json3-dev \
    libexpat1-dev \
    libssl-dev && \
    apt-get clean all

### Install tzdata package
### During ordinary installation, the package
### wants to be configured. The additional commands
### circumvene the configuration on docker
RUN apt-get update && apt-get install -y tzdata && ln -fs /usr/share/zoneinfo/America/New_York /etc/localtime && dpkg-reconfigure --frontend noninteractive tzdata


### Boost installation
RUN apt-get update && apt-get install -y libboost-all-dev

### Install C++, cmake and python
RUN apt-get update && apt-get install -y cmake g++ python3 python-is-python3 git sudo nano wget

### Install QT5 as a prerequiste of GeoModel
RUN  apt-get update &&  apt-get install -y qtbase5-dev libqt5opengl5-dev

### Install sqlite for the GeoModel outputs
RUN apt-get update && apt-get install -y sqlite3 libsqlite3-dev

### Install eigen to avoid the own installation
RUN apt-get update && apt-get install -y libeigen3-dev 

### Install hdf5 to avoid the own installation
RUN apt-get update && apt-get install -y libhdf5-dev

### Install hepmc3 to avoid the own installation
RUN apt-get update && apt-get install -y libhepmc3-dev

### Install xercesC
RUN wget https://cern.ch/lcgpackages/tarFiles/sources/xerces-c-${XercesC_VERSION}.tar.gz && \
         tar -xzf xerces-c-${XercesC_VERSION}.tar.gz && \
         mkdir build && \
         cd build && \
         cmake ../xerces-c-${XercesC_VERSION} && \
         make -j4  && \
         make install && \
         cd .. && \
         rm -rf xerces-c-${XercesC_VERSION} build


### Install the OpenGL module
RUN apt-get update && apt-get install -y mesa-utils build-essential libgl1-mesa-dev

### Install Coin
RUN pwd && \
    ls  && \
    wget https://atlas-vp1.web.cern.ch/atlas-vp1/sources/coin-${Coin_VERSION}-src.zip && \
    unzip coin-${Coin_VERSION}-src.zip -d coin-sources && \
    mv coin-sources/* coin && \
    mkdir build_coin && \
    cd build_coin && \
    cmake -DCMAKE_BUILD_TYPE=Debug -DCMAKE_INSTALL_PREFIX=$INSTALL_DIR ../coin && \
    make -j4 && \
    make install

### Install soqt
RUN pwd; ls && \
    wget https://atlas-vp1.web.cern.ch/atlas-vp1/sources/soqt.zip && \
    unzip soqt.zip && \
    mkdir build_soqt && \
    cd build_soqt && \
    cmake -DCMAKE_BUILD_TYPE=Debug -DCMAKE_INSTALL_PREFIX=$INSTALL_DIR ../soqt && \
    make -j8 && \
    make install && \
    pwd; ls  && \
    ls ../ && \
    ls ../../


# Set up the ATLAS user, and give it super user rights.
RUN addgroup wheel && \    
    echo '%wheel	ALL=(ALL)	NOPASSWD: ALL' >> /etc/sudoers && \
    adduser atlas && chmod 755 /home/atlas && \
    usermod -aG wheel atlas && \
    usermod -aG root atlas && \
    mkdir /workdir && chown "atlas:atlas" /workdir && \
    chmod 755 /workdir

USER atlas
WORKDIR /workdir



### Install Geant4
RUN pwd && \
    git clone https://gitlab.cern.ch/geant4/geant4.git && \
    cd geant4 && \
    git checkout tags/$GEANT4_VERSION && \
    mkdir build_geant4 && \
    cd build_geant4 && \
    rm -rf * && \
    cmake -DCMAKE_INSTALL_PREFIX=$INSTALL_DIR/geant4 -DGEANT4_INSTALL_DATA=ON -DGEANT4_USE_GDML=ON -DGEANT4_BUILD_MULTITHREADED=ON  ../ && \
    make -j8 > /dev/null && \
    sudo make install > /dev/null && \
    cd .. && \
    rm -rf build_geant4 geant4 && \
    ls  ~/ && \
    echo "Listing /usr/local" && \
    ls /usr/local && \
    echo "Listing /usr/local/geant4" && \
    ls /usr/local/geant4 && \
    echo "#!/bin/bash" > ~/setup_docker.sh && \
    echo "cd /usr/local/geant4/bin" >> ~/setup_docker.sh && \
    echo "source geant4.sh" >> ~/setup_docker.sh && \
    echo "cd -" >> ~/setup_docker.sh && \
    echo "Cat setup_docker.sh" && \
    cat ~/setup_docker.sh && \
    sudo mv ~/setup_docker.sh  /usr/local/geant4/setup_docker.sh && \
    echo "source /usr/local/geant4/setup_docker.sh" >> ~/.bashrc





