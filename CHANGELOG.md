## 1.1 (12-06-2020) Marilena Bandieramonte
- Remove CLHEP installation from the docker image, not needed from GeoModelG4 and FullSimLight

## 1.0 (16-05-2020) Marilena Bandieramonte
- Docker image with Ubuntu 18.04 that will be used by GeoModelG4 and FullSimLight as baseline image with Geant4 10.6 built in
